require('./bootstrap');
require('alpinejs');
var pace = require('pace-js');

var Turbolinks = require("turbolinks");
Turbolinks.start();

pace.start();

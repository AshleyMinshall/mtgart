<footer class="h-10 mt-2 sm:mt-6 lg:mt-8 px-2 sm:px-6 lg:px-8 bg-gray-700 text-white flex items-center">
    <span>MTGPics 2, because <a target="_" href="https://mtgpics.com" class="text-yellow-500">mtgpics.com</a> is broken ({{ date('Y') }}).</span>
    <button class="ml-auto" onclick="window.scrollTo({ top: 0, behavior: 'smooth'})">Back to top</button>
</footer>

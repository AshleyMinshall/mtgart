<x-app-layout>
    <x-slot name="header">How to use scryfall search?</x-slot>
    <div class="bg-white dark:bg-gray-500 p-4 rounded-md space-y-4">
        <p class="text-lg">
            My syntax is 99% the same as <a target="_blank" href="https://scryfall.com/docs/syntax"
                class="text-primary-600">Scryfall's syntax search</a>.
            If you used their search before, you should have no problems.
            However, there are some minor differences (and chances of bugs of course &#128512; ).
        </p>

        <div>
            <p class="text-lg">The syntax follows this structure:</p>
            <ul class="ml-6 list-disc">
                <li class="">&lt;keyword&gt;&lt;operator&gt;&lt;value&gt; with no spaces in between (eg. <a
                        target="_blank" href="{{ route('art') . '?scryfallSearch=name:Brainstorm' }}"
                        class="text-primary-500">name:Brainstorm</a>)</li>
                <li class="">In the case above, the keyword is <span class="font-semibold">name</span>, the operator is
                    <span class="font-semibold">":"</span> and the value is <span
                        class="font-semibold">Brainstorm</span>
                </li>
                <li class="">
                    You can have more than 1 search term, so if you wanted to see
                    all the art where the artist is "Magali Villeneuve" and the width
                    is greater than 1000px, your search can be <a target="_blank"
                        href="{{ route('art') . "?scryfallSearch=artist:\"Magali Villeneuve\" width>1000" }}"
                        class="text-primary-500">artist:"Magali Villeneuve" width>1000</a>
                </li>
            </ul>
        </div>

        <div>
            <p class="text-lg">Keywords allowed:</p>
            @php
                $keywords = [['name', 'Card name', 'name:"Goblin Ski Patrol"'], ['name (default)', "You can search for just the card name, without keyword", '"Goblin Ski Patrol"'], ['artist', 'Artist full name', 'artist="Seb Mckinnon"'], ['set', "Set name (NOTE: Set codes won't work here because of my data is stored, sorry)", 'set:Dominaria'], ['width', 'Art width in px (good for filtering out low res images)', 'width>500 width<2500'], ['height', 'Art height in px', 'height!=576'], ['mime', "Image extension (however they are all '.jpg' so don't bother with this one lol)", 'mime=jpg']];
            @endphp
            <div class="overflow-x-auto">
                <table class="w-full">
                    <thead>
                        <tr class="">
                            <th class="border p-1 font-normal text-left w-1/6">Keyword</th>
                            <th class="border p-1 font-normal text-left w-1/2">Description</th>
                            <th class="border p-1 font-normal text-left w-1/3">Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($keywords as [$keyword, $desc, $example])
                            <tr class="">
                                <td class="border p-1">{{ $keyword }}</td>
                                <td class="border p-1">{{ $desc }}</td>
                                <td class="border p-1"><a target="_blank"
                                        href="{{ route('art') . "?scryfallSearch=$example" }}"
                                        class="text-primary-500">{{ $example }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div>
            <p class="text-lg">Operators allowed:</p>
            @php
                $operators = [[':', 'Includes / Contains (partial match, LIKE operator for sql)', 'set:Dragon'], ['=', 'Equals (Exact match)', 'name=Anger'], ['<=', 'Less than or equal to', 'name<=Goblin'], ['>=', 'Greater than or equal to', 'height>=460'], ['<', 'Less than', 'name<Anchor'], ['>', 'Greater than', 'name>Anchor'], ['!=', 'Unequal to', 'name!="Uro, Titan of Nature\'s Wrath"']];
            @endphp
            <div class="overflow-x-auto">
                <table class="w-full">
                    <thead>
                        <tr class="">
                            <th class="border p-1 font-normal text-left w-1/6">Operator</th>
                            <th class="border p-1 font-normal text-left w-1/2">Description</th>
                            <th class="border p-1 font-normal text-left w-1/3">Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($operators as [$keyword, $desc, $example])
                            <tr class="">
                                <td class="border p-1">{{ $keyword }}</td>
                                <td class="border p-1">{{ $desc }}</td>
                                <td class="border p-1"><a target="_blank"
                                        href="{{ route('art') . "?scryfallSearch=$example" }}"
                                        class="text-primary-500">{{ $example }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div>
            <p class="text-lg">Extras</p>
            @php
                $extras = [['You can negate terms using a "-" (minus sign) in front of it', '-set:"Throne of Eldraine"'], ['By default terms are searched like "this and that". You can use "OR" to do a "this or that" search', 'artist:titus or artist:avon'], ['You group terms using () (brackets) when you want to use "OR"', 'through (depths or sands or mists)']];
            @endphp
            <div class="overflow-x-auto">
                <table class="w-full">
                    <thead>
                        <tr class="">
                            <th class="border p-1 font-normal text-left w-2/3">Description</th>
                            <th class="border p-1 font-normal text-left w-1/3">Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($extras as [$desc, $example])
                            <tr class="">
                                {{-- <td class="border p-1">{{ $keyword }}</td> --}}
                                <td class="border p-1">{{ $desc }}</td>
                                <td class="border p-1"><a target="_blank"
                                        href="{{ route('art') . "?scryfallSearch=$example" }}"
                                        class="text-primary-500">{{ $example }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div>
            <p class="text-lg">Extra extras</p>
            <ul class="ml-6 list-disc">
                <li class="">
                    The searcher relies on a separate python webserver that does the text parser. If it is down, it will go back to searching on name only.
                </li>
            </ul>
        </div>
    </div>
</x-app-layout>

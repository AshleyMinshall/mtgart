<!DOCTYPE html>
<html lang="en">

<head>
    <title>Demo 1</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://aframe.io/releases/1.0.0/aframe.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar.js"></script>
</head>

<body>
    <a-scene embedded vr-mode-ui="enabled: true"
        arjs="sourceType: webcam; detectionMode: mono_and_matrix; matrixCodeType: 4x4;">

        <a-marker preset="hiro">
            <a-box position="0 0.5 0" material="color: yellow;"></a-box>
        </a-marker>
        <a-entity camera></a-entity>
    </a-scene>
</body>

</html>

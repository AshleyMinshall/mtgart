<!DOCTYPE html>
<html>
<script src="https://aframe.io/releases/1.0.0/aframe.min.js"></script>
<script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar.js"></script>

<body style="margin : 0px; overflow: hidden;">
    <a-scene embedded vr-mode-ui="enabled: true"
        arjs="sourceType: webcam; detectionMode: mono_and_matrix; matrixCodeType: 4x4;">
        <a-assets>
            @foreach($deck->cards as $card)
            <img src="{{ $card->localPngImage }}" crossorigin="anonymous" id="image-{{ $card->id }}">
            @endforeach
        </a-assets>

        @foreach($deck->cards as $index => $card)
        <a-marker type="barcode" value="{{ $index }}">
            <a-image width="1.8" height="2.7" rotation="270 270 90" src="#image-{{ $card->id }}">
        </a-marker>
        @endforeach
        <a-entity camera></a-entity>
    </a-scene>
</body>

</html>

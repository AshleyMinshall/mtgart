<x-app-layout>
    {{-- <x-slot name="header">mtgAR</x-slot> --}}
    <div class="space-y-2 text-lg">
        <p>
            It would be cool to be able to play Magic with virtual cards, wouldn't it?
            Imagine not needing to shuffle your deck manually ever, or play with any
            card you wanted to, while still feeling like you were playing Magic?
        </p>

        <p>
            Well I haven't got there yet, but I think it's possible. I've built 2 demos using <a class="text-purple-500"
                target="_blank" href="https://github.com/AR-js-org/AR.js">AR-js</a>, but I have a problem; I don't know
            what I'm doing.
        </p>

        <p>
            AR technology, is foreign to me; I understand what it is, but I don't know where to move from here.
        </p>

        <p>
            If this is interesting to you, let me know, maybe we can work on it together.
        </p>

        <div>
            <p>Both demos will require 2 devices:</p>
            <ul class="list-disc ml-6 text-base">
                <li>1 device with a camera (Camera device)</li>
                <li>Another to open the marker image (Marker device. You could print the image if you want)</li>
            </ul>
            <p>You will need to give the app permission to use your camera.</p>
        </div>
    </div>

    <div class="mt-4">
        <h1>
            <a class="text-xl text-purple-600" target="_blank" href="{{ route('mtgAR.demo-1') }}">Demo 1</a>
        </h1>

        <div class="space-y-1">
            <p>
                This is a simple demo I found on AR.js's site. You will need to open
                the Hiro image below (it functions like a QR code, but has less data
                storage and is quicker to read) on your phone. You should see a yellow
                appear on the Hiro marker.
            </p>

            <p>
                I recommend opening the image in a new tab and make a qr code from
                the url, which can be scanned in by Firefox and chrome on android.
            </p>
        </div>

        <div>
            <img class="h-56 shadow-lg mx-auto mt-2" src="{{ asset('images/mtgAR/markers/hiro.png') }}"
                alt="Hiro marker">
            <p class="text-center">
                <a href="https://en.wikiversity.org/wiki/3D_Modelling/Examples/AR_with_Markers"
                    class="text-gray-600 italics mt-1 text-sm">Hiro marker</a>
            </p>
        </div>
    </div>

    <div class="mt-4">
        <h1>
            <a class="text-xl text-purple-600" target="_blank" href="{{ route('mtgAR.demo-2') }}">Demo 2</a>
        </h1>

        <div class="space-y-1">
            <p>
                Demo 2 is more interesting. Using markers that have numbers associated with them,
                different mtg cards will be imposed. Follow the same process as above,
                but this time, there are multiple markers to choice from
            </p>

            <div class="grid sm:grid-cols-2 md:grid-cols-3 gap-3">
                @foreach ($deck->cards as $index => $card)
                    <div class="grid grid-cols-2 gap-2">
                        <img class="shadow-md max-h-80" src='{{ asset("images/mtgAR/markers/cards-v3/{$index}.png") }}' alt="Index {{ $index }}">
                        <img class="shadow-md max-h-80" src="{{ asset($card->localPngImage) }}" alt="{{ $card->name }}">
                    </div>
                @endforeach
            </div>

        </div>

        {{-- <div>
            <img class="h-56 shadow-lg mx-auto mt-2" src="{{ asset('images/mtgAR/markers/hiro.png') }}"
                alt="Hiro marker">
            <p class="text-center">
                <a href="https://en.wikiversity.org/wiki/3D_Modelling/Examples/AR_with_Markers"
                    class="text-gray-600 italics mt-1 text-sm">Hiro marker</a>
            </p>
        </div> --}}
    </div>

    {{-- <div class="mt-4">
        <h1>
            <a class="text-xl text-purple-600" target="_blank" href="{{ route('mtgAR.demo-1') }}">Demo 1</a>
        </h1>

        <div>
            <p>
                This is a simple demo I found on AR.js's site. You will need to open
                the Hiro image below (it functions like a qr code, but has less data
                storage and is quicker to read) on your phone.
            </p>
        </div>
    </div> --}}
</x-app-layout>

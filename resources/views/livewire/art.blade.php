<div class="relative">
    <div class="space-y-4">
        <div class="p-2 bg-white rounded-md">
            <div class="">
                <div class="relative">
                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                        <x-heroicon-s-search class="text-gray-400 w-5 h-5" />
                    </div>

                    <input wire:model.lazy="scryfallSearch" class="form-input px-10" name="scryfall-search"
                        id="scryfall-search" type="text" placeholder="Syntax search">

                    <a title="How does this work?" target="_blank" href="{{ route('art-help') }}"
                        class="absolute inset-y-0 right-0 pr-3 flex items-center">
                        <x-heroicon-s-question-mark-circle class="h-5 w-5 text-gray-400 hover:text-gray-800" />
                    </a>
                </div>

                @if ($this->tokensToText)
                    <div class="italic mt-1 text-gray-500 text-sm">
                        {{ $this->tokensToText }}
                    </div>
                @endif
                @if($pythonSearchIsDown)
                <p class="p-1 bg-red-200 text-red-600 rounded-md mt-1">WARNING: Python search is down, reverting back to simple name search</p>
                @endif
            </div>
        </div>

        {{-- art --}}
        <div id="pagination-links-1"
            class="{{ !$this->mtgPics->hasPages() ? 'hidden' : '' }} rounded-md p-2 bg-white">
            {{ $this->mtgPics->onEachSide(2)->links() }}
        </div>

        @if (count($this->mtgPics))
            <div class="grid gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 transition-opacity"
                wire:loading.class='opacity-50' wire:target="page,cardName,setName">
                @foreach ($this->mtgPics as $mtgPic)
                    <a x-data="{...loadingImage(), ...{src: '{{ $mtgPic->url }}'}}" x-init="init()"
                        wire:click.prevent="openArt({{ $mtgPic->id }})" href="#"
                        class="rounded-md shadow-md h-56 relative overflow-hidden transition-opacity opacity-0 bg-transparent-squares"
                        x-bind:class="{'opacity-0' : loading}" wire:key="{{ $mtgPic->id }}">
                        <span class="px-1 absolute top-0 w-full shadow"
                            style="background: rgba(255, 255, 255, 0.7)">{{ $mtgPic->name }}</span>
                        <div class="rounded-md w-full h-full object-cover flex items-center">
                            <img x-ref="image" loading="lazy" x-bind:src="src"
                                alt="{{ $mtgPic->set_name }} {{ $mtgPic->name }}">
                        </div>
                        <span class="px-1 absolute bottom-0 w-full shadow sm:text-sm"
                            style="background: rgba(255, 255, 255, 0.7)">{{ $mtgPic->artist }}
                            ({{ $mtgPic->width }}x{{ $mtgPic->height }})</span>
                    </a>
                @endforeach
            </div>
        @else
            <p class="">There are no images that meet those requirements.</p>
        @endif
    </div>

    @if ($this->mtgPic)
        <x-slide-over wire:model="viewModal" width="96">
            <div class="space-y-6">
                <div>
                    <div class="block w-full rounded-lg overflow-hidden">
                        <img src="{{ $this->mtgPic->url }}" alt="" class="object-cover" />
                    </div>
                    <div class="mt-4 flex items-start justify-between">
                        <div>
                            <h2 class="text-lg font-medium text-gray-900">
                                <span class="sr-only">Details for </span>{{ $this->mtgPic->name }}
                            </h2>
                            <p class="text-sm font-medium text-gray-500">{{ $this->mtgPic->artist }}</p>
                        </div>
                        {{-- <button type="button"
                    class="ml-4 h-8 w-8 bg-white rounded-full flex items-center justify-center text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500">
                    <!-- Heroicon name: outline/heart -->
                    <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                    </svg>
                    <span class="sr-only">Favorite</span>
                </button> --}}
                    </div>
                </div>
                <div>
                    <h3 class="font-medium text-gray-900">Information</h3>
                    <dl class="mt-2 border-t border-b border-gray-200 divide-y divide-gray-200">
                        <div class="py-3 flex justify-between text-sm font-medium">
                            <dt class="text-gray-500">Set</dt>
                            <dd class="text-gray-900">{{ $this->mtgPic->set_name }}</dd>
                        </div>
                        <div class="py-3 flex justify-between text-sm font-medium">
                            <dt class="text-gray-500">Uploaded by</dt>
                            <dd class="text-gray-900">mtgpics.com</dd>
                        </div>
                        <div class="py-3 flex justify-between text-sm font-medium">
                            <dt class="text-gray-500">Created</dt>
                            <dd class="text-gray-900">{{ $this->mtgPic->created_at->format('M d, Y') }}</dd>
                        </div>
                        {{-- <div class="py-3 flex justify-between text-sm font-medium">
                    <dt class="text-gray-500">Last modified</dt>
                    <dd class="text-gray-900">June 8, 2020</dd>
                </div> --}}
                        <div class="py-3 flex justify-between text-sm font-medium">
                            <dt class="text-gray-500">Dimensions</dt>
                            <dd class="text-gray-900">{{ $this->mtgPic->width }} x {{ $this->mtgPic->height }}
                            </dd>
                        </div>

                        {{-- <div class="py-3 flex justify-between text-sm font-medium">
                    <dt class="text-gray-500">Resolution</dt>
                    <dd class="text-gray-900">72 x 72</dd>
                </div> --}}
                    </dl>
                </div>
                <div class="flex space-x-3">
                    <a href="{{ $this->mtgPic->url }}" download="{{ $this->mtgPic->name }}"
                        class="text-center flex-1 bg-indigo-600 py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Download
                    </a>
                    {{-- <button wire:click="downloadMtgPic" type="button"
                    class="flex-1 bg-indigo-600 py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Download
                </button> --}}
                    <a href="{{ $this->mtgPic->url }}" target="_"
                        class="text-center flex-1 bg-gray-600 py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">
                        Open in new tab
                    </a>
                </div>
            </div>
        </x-slide-over>
    @endIf

    @push('css')
        <style>
            .bg-transparent-squares {
                background-image: url('/images/transparent.png');
            }

        </style>
    @endpush
    @push('js')
        <script>
            function loadingImage() {
                return {
                    loading: true,
                    init: function() {
                        this.loadImage();
                    },
                    loadImage() {
                        this.loading = true;
                        var downloadImage = new Image();
                        downloadImage.onload = () => {
                            this.$refs.image.src = this.src;
                            this.loading = false;
                        };
                        downloadImage.src = this.src;
                    }
                }
            };
            // rgb(129, 140, 248)

        </script>
    @endpush
</div>

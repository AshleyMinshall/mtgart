<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardDecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_decks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('card_id');
            $table->bigInteger('deck_id');
            $table->integer('quantity');
            $table->boolean('sideboard', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_decks');
    }
}

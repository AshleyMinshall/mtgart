<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChilliAxeExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chilli_axe_extras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('illustration_id');
            $table->integer("template_1");
            $table->integer("template_2")->nullable();
            $table->json("extras")->nullable();
            $table->timestamps();

            $table->unique("illustration_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chilli_axe_extras');
    }
}

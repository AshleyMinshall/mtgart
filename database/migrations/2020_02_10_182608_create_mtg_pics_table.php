<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMtgPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtg_pics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('unique_id');
            $table->string('name');
            $table->string('mtg_pics_id');
            $table->integer('width');
            $table->integer('height');
            $table->string('set_name');
            $table->string('artist')->nullable();
            $table->string('image_url');
            $table->string('mime');
            $table->boolean('is_downloaded')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtg_pics');
    }
}

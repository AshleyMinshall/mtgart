<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddFlavorNameToCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->string('printed_name')->nullable();
            $table->string('flavor_name')->nullable();
        });
        DB::update('update cards set flavor_name = alternative_name');
        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn(['alternative_name']);
            $table->boolean('is_personal')->default(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->string('alternative_name')->nullable();
        });
        DB::update('update cards set alternative_name = flavor_name');
        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn(['printed_name', 'flavor_name', 'is_personal']);
        });
    }
}

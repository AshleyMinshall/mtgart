<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDownloadedAtColumnToMtgPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtg_pics', function (Blueprint $table) {
            $table->dropColumn('is_downloaded');
            $table->timestamp('downloaded_at')->nullable();
            $table->boolean('is_duplicate')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtg_pics', function (Blueprint $table) {
            $table->dropColumn(['downloaded_at', 'is_duplicate']);
            $table->boolean('is_downloaded')->default(false);
        });
    }
}

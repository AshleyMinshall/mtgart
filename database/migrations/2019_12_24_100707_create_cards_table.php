<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('oracle_id');
            
            // $table->bigInteger('mtgo_id')->nullable();
            // $table->bigInteger('mtgo_foil_id')->nullable();
            // $table->bigInteger('tcgplayer_id')->nullable();
            
            $table->string('name');
            // $table->string('lang');
            $table->date('released_at');

            // https://api.scryfall.com/cards/{id}
            // $table->string('uri'); 
            // https://scryfall.com/card/{set}/{collectors_number}/{kebab_case(name)}
            // $table->string('scryfall_uri');
            $table->string('layout');
            // $table->boolean('highres_image');
            
            $table->string('mana_cost')->nullable();
            $table->double('cmc')->nullable();
            
            $table->text('type');
            $table->longText('text')->nullable();
            $table->json('card_faces')->nullable();
            $table->json('all_parts')->nullable();
            $table->json('colors')->nullable();
            $table->json('color_identity')->nullable();
            
            $table->string('loyalty')->nullable();
            $table->string('power')->nullable();
            $table->string('toughness')->nullable();

            $table->string('set_code');
            $table->string('set_name');
            // $table->string('set_uri');
            
            $table->string('collector_number');
            $table->string('rarity');
            $table->text('flavor_text')->nullable();
            $table->uuid('card_back_id')->nullable();

            $table->string('artist');
            $table->uuid('illustration_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['id', 'oracle_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}

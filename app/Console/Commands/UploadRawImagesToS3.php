<?php

namespace App\Console\Commands;

use App\Models\MtgPic;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class UploadRawImagesToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload-flat-images-to-s3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Uploads flat images to s3 bucket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line("This should not be used");
        return 0;
        // $perPage = 1000;

        // $query = MtgPic::where('is_duplicate', false)->whereNotNull('downloaded_at');

        // $offset = 1;
        // $count = $query->count();

        // for ($page = 0; $page < ceil(($count - $offset) / $perPage); $page++) {
        //     $off = $offset + ($perPage * $page);
        //     $chunked = clone $query->skip($off)->take($perPage)->get();
        //     foreach ($chunked as $i => $mtgPic) {
        //         $this->line(sprintf("%s:%s", $off + $i, $mtgPic->name));
        //         if (!$mtgPic->existsOnS3) {
        //             $localFile = Storage::disk('public')->get("raw/{$mtgPic->set_name}/{$mtgPic->save_name}.{$mtgPic->mime}");

        //             $result = Storage::disk('raw-images')->put($mtgPic->saveNameMime, $localFile);
        //             if ($result) {
        //                 $this->info("Uploaded $mtgPic->saveNameMime");
        //             } else {
        //                 $this->error("Failed to upload $mtgPic->saveNameMime");
        //             }
        //         } else {
        //             $this->line("Skipping $mtgPic->saveNameMime");
        //         }
        //     }
        // }
    }
}

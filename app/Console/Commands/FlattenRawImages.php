<?php

namespace App\Console\Commands;

use App\Models\MtgPic;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class FlattenRawImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flatten-raw-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $perPage = 1000;

        $query = MtgPic::where('is_duplicate', false)->whereNotNull('downloaded_at');

        $offset = 1;
        $count = $query->count();

        for ($page = 0; $page < ceil(($count - $offset) / $perPage); $page++) {
            $off = $offset + ($perPage * $page);
            $chunked = clone $query->skip($off)->take($perPage)->get();
            foreach ($chunked as $i => $mtgPic) {
                $this->line(sprintf("%s:%s", $off + $i, $mtgPic->name));
                $existsOnFlat = Storage::disk('public')->exists("flat/{$mtgPic->saveNameMime}");

                if ($mtgPic->existsOnLocal && !$existsOnFlat) {
                    $result = Storage::disk('public')->copy(
                        "raw/{$mtgPic->set_name}/{$mtgPic->saveNameMime}",
                        "flat/{$mtgPic->saveNameMime}"
                    );

                    if ($result) {
                        $this->info("Copied $mtgPic->saveNameMime");
                    } else {
                        $this->error("Failed to copy $mtgPic->saveNameMime");
                    }
                } else {
                    $this->line("Skipping $mtgPic->saveNameMime");
                }
            }
        }
    }
}

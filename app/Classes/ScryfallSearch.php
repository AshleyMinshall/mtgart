<?php

namespace App\Classes;

use Exception;
use App\Exceptions\ScryfallSearchException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

class ScryfallSearch
{
    protected $keywords = [];
    protected $operators = [];
    protected $inputStr = "";
    protected $parsedTokens = [];

    public function __construct($keywords, $operators)
    {
        $this->keywords = $keywords;
        $this->operators = $operators;
    }

    public function parse($inputStr)
    {
        $this->inputStr = $inputStr;
        if ((count($this->keywords) > 0) && (count($this->operators) > 0) && $this->inputStr) {
            $response = Http::post(env('PYTHON_SCRYFALL_API'), [
                "keywords" => $this->keywords,
                "operators" => $this->operators,
                "input_str" => $this->inputStr
            ]);
            $this->parsedTokens = $response->json();
            return $this->parsedTokens;
            // $message = [
            //     'success' => true,
            //     'message' => 'All good',
            //     'tokens' => $this->parsedTokens,
            // ];

        } else {
            throw new ScryfallSearchException('Need to pass keywords, operators and inputStr');
        }
    }
}

<?php

namespace App\Models;

use App\Classes\IllustrationInfo;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Illustration extends Model
{
    // protected $guarded = [
    //     "id", "created_at", "updated_at", "deleted_at"
    // ];

    protected $fillable = [
        "illustration_id", "name", "mime", "is_downloaded"
    ];

    protected $casts = [
        "is_downloaded" => "boolean",
        "illustration_id" => "uuid",
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($illustration) {
            if (is_null($illustration->illustration_id)) {
                $id = Uuid::generate(4);
                $illustration->illustration_id = $id;
                $illustration->name .= "_{$id}";
            }
        });
    }

    public function chilliAxeExtra()
    {
        return $this->hasOne(ChilliAxeExtra::class);
    }

    public function createChilliAxeExtra()
    {
        $chilliAxeExtra = ChilliAxeExtra::makeFromIllustration($this);
        $this->chilliAxeExtra()->create($chilliAxeExtra->toArray());
        return $this->chilliAxeExtra;
    }

    public static function exists($illustrationId)
    {
        $i = static::where("illustration_id", $illustrationId)->first();
        return ($i) ? $i->append('assetLocation') : null;
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'illustration_id', 'illustration_id');
    }

    public function getSaveLocationAttribute()
    {
        return storage_path(sprintf('app/public/illustrations/%s.%s', $this->name, $this->mime));
    }

    public function getAssetLocationAttribute()
    {
        return asset(IllustrationInfo::$dir . "/{$this->getFileNameAttribute()}");
    }

    public function getFileNameAttribute()
    {
        return "{$this->name}.{$this->mime}";
    }
}

<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Set extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "set_id", "code", "name", "type", "count", "icon_svg_uri",
    ];

    protected $casts = [
        "is_personal" => "boolean",
        "is_wide_icon" => "boolean"
    ];

    public static $nonIllustrationTypes = [
        "funny", "memorabilia",
    ];

    public static function updateOrCreateFromScry($scry)
    {
        $scry["set_id"] = Arr::pull($scry, "id");
        $scry["type"] = Arr::pull($scry, "set_type");
        $scry["count"] = Arr::pull($scry, "card_count");
        return static::updateOrCreate(
            ["set_id" => $scry["set_id"]],
            Arr::only($scry, ["set_id", "code", "name", "type", "count", "icon_svg_uri"])
        );
    }

    public  function setIconCode()
    {
        // custom set icon
        $promoIcon = "&#xe687;"; // promo, secret lair...

        $setIconInfo = [
            // cant find in scryfall
            "2U" => "&#xe949;",
            "V0X" => "&#xe920;",
            "1E" => "&#xe947;",
            "2E" => "&#xe948;",

            // alt icon
            "ICE2" => "&#xe925;",

            // had to rename
            "FBB" => "&#xe94a;",
            "AKR" => "&#xe92d;",
            "MPS" => "&#xe913;",
            "PNEM" => "&#xe626;",
            "REN" => "&#xe917;",
            "RIN" => "&#xe918;",
            "PVAN" => "&#xe655;",

            // spelling error
            "10E" => "&#xe60b;",

            // new
            "JMP" => "&#xe96f;",
            "2XM" => "&#xe96e;",
            "IKO" => "&#xe962;",
            "SLD" => $promoIcon,
            "SLU" => $promoIcon,
            "ZNE" => "&#xe97a;",
            "ZNR" => "&#xe963;",
            "PRM" => "&#xe91b;",
            "J13" => $promoIcon,
            "J18" => $promoIcon,
            "J19" => $promoIcon,
            "PF20" => $promoIcon,



            // unchanged
            "2ED" => "&#xe602;",
            "3ED" => "&#xe603;",
            "4ED" => "&#xe604;",
            "5DN" => "&#xe633;",
            "5ED" => "&#xe606;",
            "6ED" => "&#xe607;",
            "7ED" => "&#xe608;",
            "8ED" => "&#xe609;",
            "9ED" => "&#xe60a;",
            "A25" => "&#xe93d;",
            "AER" => "&#xe90f;",
            "AKH" => "&#xe914;",
            "ALA" => "&#xe641;",
            "ALL" => "&#xe61a;",
            "APC" => "&#xe62a;",
            "ARB" => "&#xe643;",
            "ARC" => "&#xe657;",
            "ARN" => "&#xe613;",
            "ATH" => "&#xe65f;",
            "ATQ" => "&#xe614;",
            "AVR" => "&#xe64c;",
            "BBD" => "&#xe942;",
            "BFZ" => "&#xe699;",
            "BNG" => "&#xe651;",
            "BOK" => "&#xe635;",
            "BRB" => "&#xe660;",
            "BTD" => "&#xe661;",
            "C13" => "&#xe65b;",
            "C14" => "&#xe63d;",
            "C15" => "&#xe900;",
            "C16" => "&#xe910;",
            "C17" => "&#xe934;",
            "C18" => "&#xe946;",
            "C19" => "&#xe95f;",
            "C20" => "&#xe966;",
            "CC1" => "&#xe968;",
            "CHK" => "&#xe634;",
            "CHR" => "&#xe65e;",
            "CM1" => "&#xe65a;",
            "CM2" => "&#xe940;",
            "CMA" => "&#xe916;",
            "CMD" => "&#xe658;",
            "CMR" => "&#xe969;",
            "CNS" => "&#xe65c;",
            "CN2" => "&#xe904;",
            "CON" => "&#xe642;",
            "CSP" => "&#xe61b;",
            "DD2" => "&#xe66a;",
            "DDC" => "&#xe66b;",
            "DDD" => "&#xe66c;",
            "DDE" => "&#xe66d;",
            "DDF" => "&#xe66e;",
            "DDG" => "&#xe66f;",
            "DDH" => "&#xe670;",
            "DDI" => "&#xe671;",
            "DDJ" => "&#xe672;",
            "DDK" => "&#xe673;",
            "DDL" => "&#xe674;",
            "DDM" => "&#xe675;",
            "DDN" => "&#xe676;",
            "DDO" => "&#xe677;",
            "DDP" => "&#xe698;",
            "DDQ" => "&#xe908;",
            "DDR" => "&#xe90d;",
            "DDS" => "&#xe921;",
            "DDT" => "&#xe933;",
            "DDU" => "&#xe93e;",
            "DGM" => "&#xe64f;",
            "DIS" => "&#xe639;",
            "DKA" => "&#xe64b;",
            "DKM" => "&#xe662;",
            "DOM" => "&#xe93f;",
            "DPA" => "&#xe689;",
            "DRB" => "&#xe678;",
            "DRK" => "&#xe616;",
            "DST" => "&#xe632;",
            "DTK" => "&#xe693;",
            "E01" => "&#xe92d;",
            "E02" => "&#xe931;",
            "ELD" => "&#xe95e;",
            "EMA" => "&#xe903;",
            "EMN" => "&#xe90b;",
            "EVE" => "&#xe640;",
            "EVG" => "&#xe669;",
            "EXO" => "&#xe621;",
            "EXP" => "&#xe69a;",
            "FEM" => "&#xe617;",
            "FRF" => "&#xe654;",
            "FUT" => "&#xe63c;",
            "GK1" => "&#xe94b;",
            "GK2" => "&#xe959;",
            "GN2" => "&#xe964;",
            "GNT" => "&#xe94d;",
            "GPT" => "&#xe638;",
            "GRN" => "&#xe94b;",
            "GS1" => "&#xe945;",
            "GTC" => "&#xe64e;",
            "H09" => "&#xe67f;",
            "H17" => "&#xe938;",
            "HA1" => "&#xe96b;",
            "HML" => "&#xe618;",
            "HOP" => "&#xe656;",
            "HOU" => "&#xe924;",
            "ICE" => "&#xe619;",
            "IMA" => "&#xe935;",
            "INV" => "&#xe628;",
            "ISD" => "&#xe64a;",
            "J20" => "&#xe96a;",
            "JOU" => "&#xe652;",
            "JUD" => "&#xe62d;",
            "KLD" => "&#xe90e;",
            "KTK" => "&#xe653;",
            "LEA" => "&#xe600;",
            "LEB" => "&#xe601;",
            "LEG" => "&#xe615;",
            "LGN" => "&#xe62f;",
            "LRW" => "&#xe63d;",
            "M10" => "&#xe60c;",
            "M11" => "&#xe60d;",
            "M12" => "&#xe60e;",
            "M13" => "&#xe60f;",
            "M14" => "&#xe610;",
            "M15" => "&#xe611;",
            "M19" => "&#xe941;",
            "M20" => "&#xe95d;",
            "M21" => "&#xe960;",
            "MBS" => "&#xe648;",
            "MD1" => "&#xe682;",
            "ME1" => "&#xe68d;",
            "ME2" => "&#xe68e;",
            "ME3" => "&#xe68f;",
            "ME4" => "&#xe690;",
            "MED" => "&#xe94c;",
            "MH1" => "&#xe95b;",
            "MIR" => "&#xe61c;",
            "MM2" => "&#xe695;",
            "MM3" => "&#xe912;",
            "MMA" => "&#xe663;",
            "MMQ" => "&#xe625;",
            "MOR" => "&#xe63e;",
            "MP2" => "&#xe922;",
            "MPS" => "&#xe913;",
            "MRD" => "&#xe631;",
            "NEM" => "&#xe626;",
            "NPH" => "&#xe649;",
            "ODY" => "&#xe62b;",
            "OGW" => "&#xe901;",
            "ONS" => "&#xe62e;",
            "ORI" => "&#xe697;",
            "P02" => "&#xe665;",
            "PC2" => "&#xe659;",
            "PCA" => "&#xe911;",
            "PCY" => "&#xe627;",
            "PD2" => "&#xe680;",
            "PD3" => "&#xe681;",
            "PLC" => "&#xe63b;",
            "PLS" => "&#xe629;",
            "P02" => "&#xe665;",
            "POR" => "&#xe664;",
            "PTG" => "&#xe965;",
            "PTK" => "&#xe666;",
            "RAV" => "&#xe637;",
            "RIX" => "&#xe92f;",
            "RNA" => "&#xe959;",
            "ROE" => "&#xe646;",
            "RTR" => "&#xe64d;",
            "S00" => "&#xe668;",
            "S99" => "&#xe667;",
            "SCG" => "&#xe630;",
            "SHM" => "&#xe63f;",
            "SOI" => "&#xe902;",
            "SOK" => "&#xe636;",
            "SOM" => "&#xe647;",
            "SS1" => "&#xe944;",
            "SS2" => "&#xe95c;",
            "STH" => "&#xe620;",
            "TD2" => "&#xe91c;",
            "THB" => "&#xe961;",
            "THS" => "&#xe650;",
            "TMP" => "&#xe61f;",
            "TOR" => "&#xe62c;",
            "TPR" => "&#xe694;",
            "TSP" => "&#xe63a;",
            "UDS" => "&#xe624;",
            "UGL" => "&#xe691;",
            "ULG" => "&#xe623;",
            "UMA" => "&#xe958;",
            "UNH" => "&#xe692;",
            "UST" => "&#xe930;",
            "USG" => "&#xe622;",
            "V09" => "&#xe679;",
            "V10" => "&#xe67a;",
            "V11" => "&#xe67b;",
            "V12" => "&#xe67c;",
            "V13" => "&#xe67d;",
            "V14" => "&#xe67e;",
            "V15" => "&#xe905;",
            "V16" => "&#xe906;",
            "V17" => "&#xe939;",
            "VIS" => "&#xe61d;",
            "VMA" => "&#xe696;",
            "W16" => "&#xe907;",
            "W17" => "&#xe923;",
            "WAR" => "&#xe95a;",
            "WTH" => "&#xe61e;",
            "WWK" => "&#xe645;",
            "XLN" => "&#xe92e;",
            "ZEN" => "&#xe644;",
            "ZNC" => "&#xe967;"
        ];

        // add promos + tokens
        foreach (["P", "T"] as $prefix) {
            foreach ($setIconInfo as $setIcon => $code) {
                $newSetCode = "{$prefix}{$setIcon}";
                if (!array_key_exists($newSetCode, $setIconInfo)) {
                    $setIconInfo[$newSetCode] = $code;
                }
            }
        }

        //  // promos & token repeats
        //             "PRNA" => "&#xe959;",
        //             "T2XM" => "&#xe96e;",
        //             "TTHS" => "&#xe650;",


        // wide icons
        $wideIconCodes = [
            $promoIcon,
            "&#xe631;", // MRD
            "&#xe63f;", // SHM
            "&#xe622;", // USG
        ];

        $iconCode = null;

        if ($this->is_personal) $iconCode = $promoIcon;
        else $iconCode = Arr::get($setIconInfo, strtoupper($this->code), null);

        $isWideIcon = in_array($iconCode, $wideIconCodes);

        $this->icon_code = $iconCode;
        $this->is_wide_icon = $isWideIcon;

        $this->save();
    }
}

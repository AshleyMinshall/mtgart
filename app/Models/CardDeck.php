<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CardDeck extends Pivot
{
    protected $table = "card_decks";
    protected $casts = [
        "sideboard" => "boolean",
    ];
}

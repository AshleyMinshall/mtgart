<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;

class MtgPic extends Model
{
    protected $fillable = [
        'unique_id', 'name', 'mime', 'width',
        'height', 'set_name', 'artist', 'image_url',
        'mtg_pics_id', 'is_downloaded', 'save_name'
    ];
    protected $casts = [
        'is_duplicate' => 'boolean',
        'downloaded_at' => 'datetime'
    ];
    protected $dates = [
        'downloaded_at',
    ];

    public function setSaveName()
    {
        $now = Carbon::now();
        $this->downloaded_at = $now;
        $this->save_name = Str::slug($this->name) . "_{$now->timestamp}";
        return $this;
    }

    public static function hash($attributes)
    {
        $plaintext = "";
        foreach ($attributes as $attr => $value) {
            $plaintext .= $value;
        }
        return (string) Uuid::generate(5, $plaintext, Uuid::NS_DNS);
    }

    public function getPhysicalHashAttribute()
    {
        $args = collect(['name', 'mime', 'set_name', 'width', 'height'])->map(function ($attr) {
            return $this->$attr;
        })->toArray();
        return static::makePhysicalHash(...$args);
    }

    public static function makePhysicalHash($name, $mime, $set_name, $width, $height)
    {
        return "{$name}{$mime}{$set_name}{$width}{$height}";
    }

    // public function getSavePathAttribute()
    // {
    //     return "{$this->set_name}/{$this->name}.{$this->mime}";
    // }

    public function getUrlAttribute()
    {
        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk('flat-raw-images');

        if ($disk->exists("{$this->save_name}.{$this->mime}")) {
            return $disk->url("{$this->save_name}.{$this->mime}");
        } else {
            return asset("images/missing-image.png");
        }
    }

    public function getSaveNameMimeAttribute()
    {
        return "{$this->save_name}.{$this->mime}";
    }

    // public function getS3ImageAttribute()
    // {
    //     /** @var FilesystemAdapter $disk */
    //     $disk = Storage::disk('raw-images');
    //     return $disk->temporaryUrl("{$this->save_name}.{$this->mime}", Carbon::now()->addMinutes(5));
    // }

    // public function getExistsOnS3Attribute()
    // {
    //     return Storage::disk('raw-images')->exists($this->saveNameMime);
    // }

    public function getExistsAttribute()
    {
        return Storage::disk('flat-raw-images')->exists("{$this->save_name}.{$this->mime}");
    }

    public function getThumbnailUrlAttribute()
    {
        return Thumbnail::src($this->url)->smartcrop(200, 200)->url();
    }
}

<?php

namespace App\Models;

use App\Classes\IllustrationInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;

class Card extends Model
{
    use SoftDeletes;
    protected $guarded = [
        // 'id',
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $casts = [
        'id' => 'string',
        'oracle_id' => 'string',
        'illustration_id' => 'string',
        'released_at' => 'date',
        'highres_image' => 'boolean',
        'cmc' => 'double',
        'colors' => 'json',
        'color_identity' => 'json',
        'all_parts' => 'json',
        'card_faces' => 'json',
        'released_at' => 'date',
        'is_personal' => 'boolean',
        'frame_effects' => 'json',
        'produced_mana' => 'json',
        'full_art' => 'boolean',
        'textless' => 'boolean',
    ];
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($card) {
            if (is_null($card->id)) {
                $card->id = Uuid::generate(4);
            }
        });
    }

    public static $nonCardLayouts = [
        "token", "emblem", "art_series", "double_faced_token", "augment"
    ];

    public function set()
    {
        return $this->belongsTo(Set::class, "set_code", "code");
    }

    public function decks()
    {
        return $this->belongsToMany(Deck::class, "card_decks")
            ->using(CardDeck::class)
            ->withPivot('quantity', 'sideboard')->withTimestamps();
    }

    public function illustration()
    {
        return $this->belongsTo(Illustration::class, 'illustration_id', 'illustration_id');
    }

    public function getRawIllustrations($cache = true, $index)
    {
        return illustrationInfo::make($this, $index)->all($cache);
    }

    public function getIllustrationInfosAttribute()
    {
        return IllustrationInfo::make($this)->info();
    }

    public function getImageUrlAttribute()
    {
        return sprintf(
            "https://img.scryfall.com/cards/large/front/%s/%s/%s.jpg",
            $this->id[0],
            $this->id[1],
            $this->id
        );
    }

    public function getPngImageAttribute()
    {
        return sprintf(
            "https://c1.scryfall.com/file/scryfall-cards/png/front/%s/%s/%s.png",
            $this->id[0],
            $this->id[1],
            $this->id
        );
    }

    public function getLocalPngImageAttribute()
    {
        return asset("images/mtgAR/cardArt/$this->id.png");
    }

    public function getScryfallUrlAttribute()
    {
        return sprintf(
            "https://scryfall.com/card/%s/%s/%s",
            $this->set_code,
            $this->collector_number,
            Str::slug($this->name, '-')
        );
    }

    // public function getIllustrationUrlAttribute()
    // {
    //     return asset(sprintf("storage/illustrations/%s_%s.jpg", Str::slug($this->name), $this->illustration_id));
    // }

    public static function scryToModel($scry)
    {
        $same_keys = [
            'id', 'oracle_id', 'name', 'released_at',
            'layout', 'mana_cost', 'cmc', 'colors',
            'color_identity', 'set_name', 'flavor_text', 'artist', 'illustration_id',
            'collector_number', 'rarity', 'loyalty', 'power', 'toughness',
            'related_cards', 'card_back_id', 'flavor_name', 'printed_name',
            'frame_effects', 'produced_mana', 'full_art', 'textless', 'border_color',
        ];
        $all_parts = Arr::get($scry, 'all_parts');
        if ($all_parts) {
            $all_parts = Collection::make($all_parts)->map(function ($part) {
                return Arr::only($part, ['id', 'component']);
            })->toArray();
        }
        $card_faces = Arr::get($scry, 'card_faces');
        if ($card_faces) {
            $card_faces = Collection::make($card_faces)->map(function ($face) use ($scry) {
                if (!Arr::has($face, "illustration_id")) {
                    $face['illustration_id'] = Uuid::generate(3, $scry["id"], Uuid::NS_DNS)->string;
                }
                return Arr::except($face, ['image_uris']);
            })->toArray();
        }
        return array_merge(Arr::only($scry, $same_keys), [
            'type' => Arr::get($scry, 'type_line'),
            'text' => Arr::get($scry, 'oracle_text'),
            'all_parts' => $all_parts,
            'card_faces' => $card_faces,
            'set_code' => Arr::get($scry, 'set'),
        ]);
    }

    public static function firstOrCreateFromScry($scry)
    {
        return static::firstOrCreate(Arr::only($scry, ['id']), static::scryToModel($scry));
    }

    public static function updateOrCreateFromScry($scry)
    {
        return static::updateOrCreate(Arr::only($scry, ['id']), static::scryToModel($scry));
    }

    public static function makeFromScry($scry)
    {
        return static::make(static::scryToModel($scry));
    }

    public static function createFromScry($scry)
    {
        $model = static::makeFromScry($scry);
        $model->save();
        return $model;
    }

    // public static function isolateManaSymbols($text)
    // {
    //     return [];
    // }

    // public static function determineCMC($manaCost)
    // {
    //     // $manaSymbols = static::isolateManaSymbols($manaCost);
    //     return 0;
    // }

    // public static function determineColorIdentity($manaCost, $text)
    // {
    //     return [];
    // }
}

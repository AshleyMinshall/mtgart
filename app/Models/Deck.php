<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Deck extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at',
        // 'created_by'
    ];

    protected $casts = [
        'cards.pivot.sideboard' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function cards()
    {
        return $this->belongsToMany(Card::class, "card_decks")
            ->using(CardDeck::class)->withPivot('quantity', 'sideboard')->withTimestamps();
    }

    public function getFrontendData()
    {
        $this->cards->transform(function ($card) {
            return $card->append(['imageUrl', 'illustrationInfos']);
        });
        return $this;
    }

    public function toPrintJson()
    {
        $deck = [];
        $this->load('cards', 'cards.set');

        $standardFields = Collection::make([
            'artist', 'id', 'layout', 'loyalty', 'mana_cost',
            'name', 'text', 'power', 'toughness', 'type', 'types', 'rarity',
            'card_faces', 'flavor_name', 'printed_name', 'illustration_id',
            'color_identity',
            'frame_effects', 'produced_mana', 'full_art', 'textless', 'border_color',
        ]);

        $nonStandardMapping = [
            'number' => 'collector_number',
            'original_text' => 'text',
            'original_type' => 'type',
            'set' => 'set_code',
            'flavor' => 'flavor_text',
            'set_icon_code' => 'set.icon_code',
            'set_icon_is_wide' => 'set.is_wide_icon',
        ];

        foreach ($this->cards as $card) {
            $allCards = [];
            if ($card->layout == "transform") {
                foreach ($card->card_faces as $face) {
                    $newCard = clone $card;
                    foreach ($face as $key => $value) {
                        $newCard->{$key} = $value;
                    }
                    $newCard->type = $face['type_line'];
                    $newCard->text = $face['oracle_text'];
                    $newCard->color_identity = $face['colors'];
                    $allCards[] = $newCard;
                }
            } else {
                if ($card->layout == "meld") {
                    $meldResultId = collect($card->all_parts)->filter(function ($part) {
                        return $part['component'] == "meld_result";
                    })->first()["id"];
                    if (!array_key_exists($meldResultId, $allCards)) {
                        $allCards[$meldResultId] = Card::find($meldResultId);
                    }
                } else if ($card->layout == "split") {
                    $card->card_faces = collect($card->card_faces)->map(function ($cardFace) {
                        $illustration = Illustration::where('illustration_id', $cardFace['illustration_id'])->first();
                        $cardFace['image_location'] = is_null($illustration) ? null : $illustration->save_location;
                        return $cardFace;
                    });
                }
                $allCards[] = $card;
            }
            foreach ($allCards as $allCard) {
                $load = ["set"];
                if ($allCard->layout != "split")
                    $load[] = 'illustration';
                $allCard->load(...$load);
                $attributes = $standardFields->mapWithKeys(function ($field) use ($allCard) {
                    return [$field => $allCard->{$field}];
                });
                foreach ($nonStandardMapping as $theirs => $ours) {
                    $attributes[$theirs] = Arr::get($allCard->toArray(), $ours);
                }
                if (($allCard->relationLoaded('illustration')) && !is_null($allCard->illustration))
                    $attributes['image_location'] = $allCard->illustration->save_location;
                else
                    $attributes['image_location'] = null;
                $attributes['set'] = Str::upper($attributes['set']);
                $attributes['count'] = $allCard->set->count;
                $attributes["mana_cost"] = ($attributes["mana_cost"] == "") ? null : $attributes["mana_cost"];

                $deck[] = $attributes;
            }
        }

        foreach ($deck as $cardJson) {
            if (is_null($cardJson['set_icon_code'])) {
                dd($cardJson, $this->name);
            }
            $illus = Illustration::where("illustration_id", $cardJson['illustration_id'])
                ->with("chilliAxeExtra")->first();

            if ($illus) {
                if (is_null($illus->chilliAxeExtra)) {
                    $chilliAxeExtra = $illus->createChilliAxeExtra();
                } else $chilliAxeExtra = $illus->chilliAxeExtra;
            } else $chilliAxeExtra = null;

            if ($chilliAxeExtra) $cardJson['chilliAxeExtra'] = $chilliAxeExtra->only(["template_1", "template_2"]);
            else $cardJson["chillAxeExtra"] = null;
        }

        return json_encode($deck, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    public function downloadPngImages()
    {
        $this->loadMissing('cards');
        $client = new Client();
        foreach ($this->cards as $card) {
            $filePath = storage_path("app/public/cardArt/$card->id.png");
            if (!file_exists($filePath))
                $client->request('GET', $card->pngImage, ['sink' => $filePath]);
        }
    }
}

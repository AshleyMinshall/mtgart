<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChilliAxeExtra extends Model
{
    const SIZES = [
        "full_art" =>     [3288, 4488],
        "borderless" =>   [3288, 4021],
        "extended" =>     [3288, 2330],
        "normal" =>       [2272, 1656],
    ];

    protected $fillable = [
        "template_1", "template_2", "extras", "illustration_id"
    ];
    protected $casts = [
        "template_1" => "integer",
        "template_2" => "integer",
        "extras" => "json",
    ];

    public function illustration()
    {
        return $this->belongsTo(Illustration::class);
    }

    public static function scaleToFill($src, $dest)
    {
        return max([$dest[0] / $src[0], $dest[1] / $src[1]]);
    }

    public static function pixelsLost($src, $dest)
    {
        return [$dest[0] - $src[0], $dest[1] - $src[1]];
    }

    public static function percentageLost($src, $dest)
    {
        $lp = static::pixelsLost($src, $dest);
        return [$lp[0] / $src[0], $lp[1] / $src[1]];
    }

    public static function scaleData($src, $dest)
    {
        $fillScale = static::scaleToFill($src, $dest);
        $newSize = [$src[0] * $fillScale, $src[1] * $fillScale];
        return [
            "fillScale" => $fillScale,
            "newSize" => $newSize,
            "lostPixels" => static::pixelsLost($newSize, $dest),
            "lostPercentage" => static::percentageLost($newSize, $dest),
        ];
    }

    public static function makeFromIllustration($illustration)
    {
        $imageSize = getimagesize($illustration->saveLocation);
        $sizeData = [];
        foreach (static::SIZES as $size => $dimensions) {
            $data = static::scaleData($imageSize, $dimensions);
            $data["absMaxLp"] = collect($data["lostPercentage"])->map(fn ($p) => abs($p))->max();
            $sizeData[$size] = $data;
        }

        $bestFits = collect($sizeData)->sortBy("absMaxLp")->slice(0, 2)->keys();

        $template1Id = static::getTemplateId($bestFits[0]);
        $template2Id = static::getTemplateId($bestFits[1]);

        return static::make([
            'template_1' => $template1Id,
            'template_2' => $template2Id,
        ]);
    }

    public static function getTemplateId($size)
    {
        return array_search($size, array_keys(static::SIZES));
    }

    public function getTemplate1StringAttribute()
    {
        return array_keys(static::SIZES)[$this->template_1];
    }

    public function getTemplate2StringAttribute()
    {
        return array_keys(static::SIZES)[$this->template_2];
    }
}

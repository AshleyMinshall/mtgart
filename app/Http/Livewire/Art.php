<?php

namespace App\Http\Livewire;

use App\Classes\ScryfallSearch;
use App\Exceptions\ScryfallSearchException;
use App\Models\MtgPic;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Art extends Component
{
    use WithPagination;

    public $setName;
    public $setNames = [];
    public $cardName;

    public $viewModal = false;
    public $mtgPicId = null;

    public $scryfallSearch;

    public $queryString = ['viewModal', 'mtgPicId', 'scryfallSearch'];
    public $pythonSearchIsDown = false;

    public function mount()
    {
    }

    public function updating($field)
    {
        if (in_array($field, ['scryfallSearch'])) {
            $this->resetPage();
        }
    }

    public function render()
    {
        return view('livewire.art')->layout('layouts.app');
    }

    public function getTokensProperty()
    {
        try {
            $scryfallSearch = new ScryfallSearch(
                ['artist', 'width', 'height', 'name', 'set', 'mime'],
                ['>=', '<=', '!=', ':', '=', '>', '<']
            );
            return $scryfallSearch->parse($this->scryfallSearch);
        } catch (ScryfallSearchException $e) {
            return null;
        } catch (ConnectionException $e) {
            $this->pythonSearchIsDown = true;
            return [
                'type' => 'group',
                'is_or' => false,
                'is_negative' => false,
                'tokens' => [
                    [
                        'type' => 'single',
                        'is_or' => false,
                        'is_negative' => false,
                        'keyword' => 'name',
                        'operator' => ':',
                        'value' => $this->scryfallSearch
                    ]
                ]
            ];
        }
    }

    public function getMtgPicsProperty()
    {
        $mtgPics = MtgPic::whereNotNull('downloaded_at')->orderBy('name');
        $scryfallMessages = [];

        if ($this->tokens) {
            $mtgPics = $this->parseToken($mtgPics, $this->tokens);
        }

        $mtgPics = $mtgPics->paginate(60);
        return $mtgPics;
    }

    // protected function sortMyMenus($parentId, $menus) {
    //     $children = [];
    //     foreach ($menus as $menu) {
    //         if ($menu->id == $parentId) {
    //             $children[$menu->id] = [
    //                 'menu' => $menu,
    //                 'children' => $this->sortMyMenus($menu->id, $menus)
    //             ];
    //         }
    //     }
    //     return $children;
    // }

    protected function parseToken($query, $token)
    {
        $boolean = [$token['is_or'] ? 'or' : 'and'];
        if ($token['is_negative']) $boolean[] = 'not';
        $boolean = implode(" ", $boolean);

        if ($token['type'] == 'group') {
            $query = $query->where(function ($q) use ($token) {
                foreach ($token['tokens'] as $t) {
                    $q = $this->parseToken($q, $t);
                }
                return $q;
            }, null, null, $boolean);
        } elseif ($token['type'] == 'single') {
            // ['artist', 'width', 'height', 'name', 'set', 'mime'],
            $keywordMap = [
                'artist' => 'artist',
                'width' => 'width',
                'height' => 'height',
                'name' => 'name',
                'set' => 'set_name',
                'mime' => 'mime',
            ];
            if ($token['operator'] == ':') {
                $query->where($keywordMap[$token['keyword']], 'like', "%{$token['value']}%", $boolean);
            } else {
                $query->where($keywordMap[$token['keyword']], $token['operator'], $token['value'], $boolean);
            }
        }
        return $query;
    }

    public function getMtgPicProperty()
    {
        return $this->mtgPics->where('id', $this->mtgPicId)->first();
    }

    public function openArt($id)
    {
        $this->mtgPicId = $id;
        $this->viewModal = true;
    }

    public function downloadMtgPic()
    {
        // return response()->download(storage_path('app/public/raw/5th Edition/abyssal-specter_1592916574.jpg'));
        // return Storage::disk('public')->download('raw/5th Edition/abyssal-specter_1592916574.jpg');
        // return Storage::disk('public')->download('');
    }

    public function getTokensToTextProperty()
    {
        if ($this->tokens) {
            $text = $this->tokenToText($this->tokens);
            $text = substr($text, 2, strlen($text) - 3);
            return "where $text";
        } else {
            return "";
        }
    }

    protected function tokenToText($token, $isFirst = true)
    {
        $text = "";
        $boolean = $token['is_or'] ? 'or' : 'and';

        // if ($token['is_negative']) $boolean[] = 'not';
        // $boolean = implode(" ", $boolean);

        if ($token['type'] == 'group') {
            $groupPrefix = [""];
            if (!$isFirst) $groupPrefix[] = $boolean;
            if ($token['is_negative']) $groupPrefix[] = "not";
            // if (!$isFirst) {
            //     dd($groupPrefix, $token);
            // }
            $groupPrefix = implode(" ", $groupPrefix) . " ";
            $text .= "$groupPrefix(";
            foreach ($token['tokens'] as $i => $t) {
                $text .= $this->tokenToText($t, $i == 0);
            }
            $text .= ")";
        } elseif ($token['type'] == 'single') {
            // ['artist', 'width', 'height', 'name', 'set', 'mime'],
            $keywordMap = [
                'artist' => 'artist name',
                'width' => 'width',
                'height' => 'height',
                'name' => 'name',
                'set' => 'set name',
                'mime' => 'mime',
            ];
            // $operatorMap = [
            //     '>=' => 'greater than or equal to',
            //     '<=' => 'less than or equal to',
            //     '!=' => 'unequal to',
            //     ':' => 'includes',
            //     '=' => 'equals',
            //     '>' => 'greater than',
            //     '<' => 'less than'
            // ];
            $operatorMap = [
                '>=' => ['<', '>='],
                '<=' => ['>', '<='],
                '!=' => ['=', '!='],
                ':' => ["doesn't contain", "contains"],
                '=' => ['!=', '='],
                '>' => ['<=', '>'],
                '<' => ['>=', '<']
            ];

            $text .= sprintf(
                "%sthe %s %s \"%s\"",
                $isFirst ? "" : " $boolean ",
                $keywordMap[$token['keyword']],
                $operatorMap[$token['operator']][!$token['is_negative']],
                strtolower($token['value'])
            );
        }
        return $text;
    }
}

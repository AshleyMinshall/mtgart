<?php

use App\Http\Livewire\Art;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('art', Art::class)->name('art');
Route::view('art-help', 'art.help')->name('art-help');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::group(['prefix' => 'mtgAR'], function () {
    Route::get('/', function () {
        $deck = App\Models\Deck::with('cards')->find(46);
        return view('mtgAR.home', [
            'deck' => $deck
        ]);
    })->name('mtgAR.home');
    Route::view('demo-1', 'mtgAR.demo-1')->name('mtgAR.demo-1');
    Route::get('demo-2', function () {
        $deck = App\Models\Deck::with('cards')->find(46);

        return view('mtgAR.demo-2', [
            'deck' => $deck
        ]);
    })->name('mtgAR.demo-2');
});

// Route::get('mtgAR', function () {
//     return view('mtgAR.home');
// });

if (config('app.env') == "local") {
    Route::get('/test', function () {
        $scryfallSearch = new \App\Classes\ScryfallSearch(
            [],
            []
        );
        dd($scryfallSearch->parse("Tarmogoyf \"Seek through time\" type:legendary (type:goblin or type:elf or type:fish)"));
    });
}
